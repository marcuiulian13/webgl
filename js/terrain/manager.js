(function () {
    "use strict";

    var ChunkManager = {};

    ChunkManager.WORKERS = 5;

    ChunkManager._seed = 0.42913886439055204;

    ChunkManager._lastWorkerUsed = 0;
    ChunkManager._workers = [];

    ChunkManager._chunks = {};

    for (var i = 0; i < ChunkManager.WORKERS; i++) {
        ChunkManager._workers.push(new Worker("./js/worker/chunk.js"));
    }

    for (var i = 0; i < ChunkManager._workers.length; i++) {
        ChunkManager._workers[i].onmessage = function (message) {
            var vertices = message.data.vertices,
                colors = message.data.colors,
                normals = message.data.normals,
                indices = message.data.indices,
                tX = message.data.tX,
                tZ = message.data.tZ,
                hasWater = message.data.hasWater;

            var mesh = Game.Mesh.generateFromArrayObject(vertices, colors, normals, indices);
            var chunk = new Game.Chunk(tX, tZ, hasWater, mesh);
            ChunkManager._chunks[tX + "," + tZ] = chunk;
        };
    }

    ChunkManager.generate = function (tX, tZ, scale) {
        if (ChunkManager._chunks[tX + "," + tZ]) {
            return;
        }

        ChunkManager._chunks[tX + "," + tZ] = true;
        ChunkManager._sendToWorker(tX, tZ, scale);
    };

    ChunkManager._sendToWorker = function (tX, tZ, scale) {
        ChunkManager._lastWorkerUsed++;
        ChunkManager._lastWorkerUsed %= ChunkManager._workers.length;

        ChunkManager._workers[ChunkManager._lastWorkerUsed].postMessage({
            "seed": ChunkManager._seed,
            "tX": tX,
            "tZ": tZ,
            "size": Game.Chunk.SIZE,
            "scale": scale,
            "worker": ChunkManager._lastWorkerUsed
        });
    };

    ChunkManager.updateChunks = function (pX, pZ) {
        var chunks = ChunkManager._chunks,
            maxDist = Math.ceil(Game.camera.far / Game.Chunk.SIZE) + 1;

        for(var key in chunks) {
            var chunk = chunks[key];

            if(typeof chunk === "object") {
                var chunkDist = Math.floor(Math.sqrt((pX - chunk.tX)*(pX - chunk.tX) + (pZ - chunk.tZ)*(pZ - chunk.tZ)));
                if(chunkDist > maxDist) {
                    ChunkManager.remove(chunk.tX, chunk.tZ);
                }
            }
        }
    };

    ChunkManager.remove = function (tX, tZ) {
        var gl = Game.gl,
            chunks = ChunkManager._chunks;

        var chunk = chunks[tX + "," + tZ];

        if(!chunk) return;

        var mesh = chunk.mesh;

        if (mesh.vbo) {
            gl.deleteBuffer(mesh.vbo);
        }
        if (mesh.cbo) {
            gl.deleteBuffer(mesh.cbo);
        }
        if (mesh.nbo) {
            gl.deleteBuffer(mesh.nbo);
        }
        if (mesh.ibo) {
            gl.deleteBuffer(mesh.ibo);
        }

        delete ChunkManager._chunks[tX + "," + tZ];
    };

    Game.ChunkManager = ChunkManager;
})();