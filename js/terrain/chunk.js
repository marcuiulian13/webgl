(function () {
    "use strict";

    var Chunk = function (tX, tZ, hasWater, mesh) {
        Game.Model.call(this, mesh);

        this.tX = tX;
        this.tZ = tZ;
        this.hasWater = hasWater;
        this.transform = this.transform.x(
            Matrix.Translation($V([tX * Chunk.SIZE, 0, tZ * Chunk.SIZE])).ensure4x4());
    };

    Chunk.prototype = Object.create(Game.Model.prototype);

    Chunk.prototype.setUniforms = function () {
        Game.Model.prototype.setUniforms.call(this);

        var gl = Game.gl,
            shader = Game.Shader.current,
            pMatrix = Game.camera.getProjMatrix(),
            vMatrix = Game.camera.getViewMatrix();

        gl.uniformMatrix4fv(shader.getUniform("uTVMatrix"), false, new Float32Array(vMatrix.transpose().flatten()));
        gl.uniformMatrix4fv(shader.getUniform("uIPMatrix"), false, new Float32Array(pMatrix.inverse().flatten()));
    };

    Chunk.SIZE = 64;

    Game.Chunk = Chunk
})();