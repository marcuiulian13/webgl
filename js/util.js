(function () {
    "use strict";

    var Util = {};

    Util.readFile = function (path) {
        var deferred = Q.defer();

        var reader = new XMLHttpRequest();
        reader.open('GET', path);
        reader.onreadystatechange = function() {
            if(reader.readyState === XMLHttpRequest.DONE) {
                if(reader.status === 200) {
                    deferred.resolve(reader.responseText);
                } else {
                    deferred.reject(new Error("File not found"));
                }
            }
        };
        reader.send();

        return deferred.promise;
    };

    Game.Util = Util;
})();