(function () {
    "use strict";

    Game.Lights = {};

    Game.Lights.ambient = {
        "weight": 0.2,
        "color": [1.0, 1.0, 1.0]
    };

    Game.Lights.directional = {
        "dir": [0.5, -1.0, 0.5],
        "color": [1.0, 1.0, 1.0]
    };

    Game.initGL = function () {
        if (!Game.gl) return;
        var gl = Game.gl;

        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        gl.enable(gl.DEPTH_TEST);
        gl.depthFunc(gl.LEQUAL);

        gl.enable(gl.CULL_FACE);
        gl.enable(gl.BLEND);
        gl.cullFace(gl.BACK);

        Game.camera = new Game.Camera();

        Game.drawMode = gl.GL_TRIANGLES;

        Game.sky = new Game.Skybox();

        Game.Shader.basic = new Game.Shader("base");
        Game.Shader.terrain = new Game.Shader("terrain");
        Game.Shader.water = new Game.Shader("water");
        Game.Shader.sky = new Game.Shader("skybox");
    };

    Game.draw = function () {
        var gl = Game.gl;

        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        Game.Shader.sky.use();
        if (Game.Shader.current && Game.Shader.current.loaded && Game.Shader.current.name === "skybox") {
            Game.sky.setUniforms();
            Game.sky.draw();
        }

        Game.Shader.terrain.use();
        if (Game.Shader.current && Game.Shader.current.loaded && Game.Shader.current.name === "terrain") {
            for (var key in Game.ChunkManager._chunks) {
                var chunk = Game.ChunkManager._chunks[key];

                if (typeof chunk === "object") {
                    chunk.setUniforms();
                    chunk.draw();
                }
            }
        }
    };
})();