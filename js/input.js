(function () {
    "use strict";

    var Input = { _keys: {} };

    document.onkeydown = function (event) {
        Input._keys[event.keyCode] = true;
    };
    document.onkeyup = function (event) {
        Input._keys[event.keyCode] = false;
    };

    Input.KEYS = {
        "LEFT": 37,
        "UP": 38,
        "RIGHT": 39,
        "DOWN": 40,
        "A": 65,
        "W": 87,
        "D": 68,
        "S": 83,
        "E": 69,
        "Q": 81,
        "P": 80
    };

    Input.isKeyDown = function (keyCode) {
        return this._keys[keyCode] || false;
    };

    Input.isKeyUp = function (keyCode) {
        return !this._keys[keyCode] || true;
    };

    Game.Input = Input;
})();