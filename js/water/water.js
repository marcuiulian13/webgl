(function () {
    "use strict";

    var Water = function (tX, tZ, mesh) {
        Game.Model.call(this, mesh);
        this.tX = tX;
        this.tZ = tZ;

        this.transform = this.transform.x(
            Matrix.Translation($V([tX * Game.Chunk.SIZE, 0, tZ * Game.Chunk.SIZE])).ensure4x4());
    };

    Water.prototype = Object.create(Game.Model.prototype);

    Game.Water = Water;
})();