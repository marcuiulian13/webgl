(function () {
    "use strict";

    var WaterGenerator = {};

    WaterGenerator.generate = function (tX, tZ) {
        var size = Game.Chunk.SIZE;

        var vertices = new Float32Array([
                0, 0, 0,
                0, 0, size,
                size, 0, size,
                size, 0, 0
            ]),
            colors = new Float32Array([
                0, 0, 1,
                0, 0, 1,
                0, 0, 1,
                0, 0, 1
            ]),
            normals = new Float32Array([
                0, 1, 0,
                0, 1, 0,
                0, 1, 0,
                0, 1, 0
            ]),
            indices = new Uint16Array([
                0, 1, 2,
                0, 2, 3
            ]);

        var mesh = Game.Mesh.generateFromArrayObject(vertices, colors, normals, indices);
        return new Game.Water(tX, tZ, mesh);
    };

    Game.WaterGenerator = WaterGenerator;
})();