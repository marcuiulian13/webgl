(function () {
    "use strict";

    Game.time = 0;

    Game.update = function () {
        Game.time += 0.1;

        Game.camera.update();

        var gl = Game.gl;
        if(Game.Input.isKeyDown(Game.Input.KEYS.P)) {
            Game.drawMode = gl.LINES;
        } else {
            Game.drawMode = gl.TRIANGLES;
        }

        // temp, will be player's coordinates
        var pos = Game.camera._position,
            numChunks = Math.ceil(Game.camera.far / Game.Chunk.SIZE) + 1;

        var pX = Math.floor(-pos.x / Game.Chunk.SIZE),
            pZ = Math.floor(-pos.z / Game.Chunk.SIZE);

        for(var z = 0; z <= 2 * numChunks; z++) {
            for(var x = 0; x <= 2 * numChunks; x++) {
                var hX = x - numChunks,
                    hZ = z - numChunks;

                var chunkDist = Math.floor(Math.sqrt(hX * hX + hZ * hZ));

                if(chunkDist <= numChunks) {
                    var tX = pX - numChunks + x,
                        tZ = pZ - numChunks + z;

                    Game.ChunkManager.generate(tX, tZ, 1);
                }
            }
        }
        Game.ChunkManager.updateChunks(pX, pZ);
    };
})();