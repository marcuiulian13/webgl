(function () {
    "use strict";

    var Skybox = function () {
        var vertices = [
            -1, -1,
             1, -1,
             1,  1,
            -1,  1
        ];

        var indices = [
            0, 1, 2,
            0, 2, 3
        ];

        this.mesh = Game.Mesh.generateFromArray(vertices, undefined, undefined, indices);
    };

    Skybox.prototype.draw = function () {
        var gl = Game.gl,
            mesh = this.mesh;

        var shader = Game.Shader.current;

        gl.disable(gl.DEPTH_TEST);

        gl.enableVertexAttribArray(shader.getAttribute("aPosition"));
        gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vbo);
        gl.vertexAttribPointer(shader.getAttribute("aPosition"), 2, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, mesh.ibo);
        gl.drawElements(Game.drawMode, mesh.size, gl.UNSIGNED_SHORT, 0);

        gl.enable(gl.DEPTH_TEST);
    };

    Skybox.prototype.setUniforms = function () {
        var gl = Game.gl,
            pMatrix = Game.camera.getProjMatrix(),
            vMatrix = Game.camera.getViewMatrix();

        var shader = Game.Shader.current;

        gl.uniformMatrix4fv(shader.getUniform("uIPMatrix"), false, new Float32Array(pMatrix.inverse().flatten()));
        gl.uniformMatrix4fv(shader.getUniform("uTVMatrix"), false, new Float32Array(vMatrix.transpose().flatten()));
    };

    Game.Skybox = Skybox;
})();