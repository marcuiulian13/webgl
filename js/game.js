"use strict";

var Game = {};

Game.FPS = 60;
Game.WIDTH = 800;
Game.HEIGHT = 600;

window.onload = function () {
    var canvas = document.getElementById("canvas");
    canvas.width = Game.WIDTH;
    canvas.height = Game.HEIGHT;
    Game.gl = getWebGLContext(canvas);

    Game.start();
};

Game.start = function () {
    Game.initGL();
    window.onEachFrame(Game.run);
};

Game.run = (function () {
    var loops = 0, skipTicks = 1000 / Game.FPS,
        maxFrameSkip = 10,
        nextGameTick = new Date().getTime();

    var lastFPSCount = new Date().getTime(),
        frames = 0;

    return function () {
        loops = 0;

        while (new Date().getTime() > nextGameTick && loops < maxFrameSkip) {
            Game.update();
            nextGameTick += skipTicks;
            loops++;
        }

        Game.draw();

        frames++;
        if ((new Date).getTime() > lastFPSCount + 1000) {
            Game.fps = frames;

            lastFPSCount += 1000;
            frames = 0;
        }
    };
})();

function getWebGLContext(canvas) {
    var gl;

    try {
        gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
    } catch (e) {
        console.log("Excaption caught while getting canvas context.", e);
    }

    if (!gl) {
        alert("Unable to initialize WebGL. Your browser may not support it.");
        gl = null;
    }

    return gl;
}

(function () {
    var onEachFrame;
    if (window.requestAnimationFrame) {
        onEachFrame = function (cb) {
            var _cb = function () {
                cb();
                requestAnimationFrame(_cb);
            };
            _cb();
        };
    } else if (window.webkitRequestAnimationFrame) {
        onEachFrame = function (cb) {
            var _cb = function () {
                cb();
                webkitRequestAnimationFrame(_cb);
            };
            _cb();
        };
    } else if (window.mozRequestAnimationFrame) {
        onEachFrame = function (cb) {
            var _cb = function () {
                cb();
                mozRequestAnimationFrame(_cb);
            };
            _cb();
        };
    } else {
        onEachFrame = function (cb) {
            setInterval(cb, 1000 / 60);
        }
    }

    window.onEachFrame = onEachFrame;
})();