(function () {
    "use strict";

    var Shader = function (name) {
        var gl = Game.gl,
            self = this;

        this.name = name;
        this.loaded = false;

        var vsPromise = Shader.getShader(name + ".vs", "vs");
        var fsPromise = Shader.getShader(name + ".fs", "fs");

        Q.all([vsPromise, fsPromise])
            .then(function (results) {
                var vShader = results[0],
                    fShader = results[1],
                    id = gl.createProgram();

                gl.attachShader(id, vShader.id);
                gl.attachShader(id, fShader.id);
                gl.linkProgram(id);

                if (!gl.getProgramParameter(id, gl.LINK_STATUS)) {
                    throw new Error("Unable to initialize the shader program.");
                }

                self.id = id;
                gl.useProgram(self.id);

                var uniforms = vShader.uniforms.concat(fShader.uniforms),
                    attributes = vShader.attributes.concat(fShader.attributes);

                self._initUniforms(uniforms);
                self._initAttributes(attributes);

                self.loaded = true;
            }, function () {
                throw new Error("Shader error");
            });
    };

    Shader.prototype._initUniforms = function (uniforms) {
        var gl = Game.gl;

        this.uniforms = {};

        for(var i = 0; i < uniforms.length; i++) {
            var uniform = uniforms[i];
            var location = gl.getUniformLocation(this.id, uniform);

            this.uniforms[uniform] = location;
        }
    };

    Shader.prototype._initAttributes = function (attributes) {
        var gl = Game.gl;

        this.attributes = {};

        for(var i = 0; i < attributes.length; i++) {
            var attribute = attributes[i];
            var location = gl.getAttribLocation(this.id, attribute);

            this.attributes[attribute] = location;
        }
    };

    Shader.prototype.getUniform = function (name) {
        return this.uniforms[name];
    };

    Shader.prototype.getAttribute = function (name) {
        return this.attributes[name];
    };

    Shader.prototype.use = function () {
        if (!this.id) return;
        var gl = Game.gl;

        if(Game.Shader.current) {
            for(var attr in Game.Shader.current.attributes) {
                gl.disableVertexAttribArray(Game.Shader.current[attr]);
            }
            gl.useProgram(null);
        }

        Game.Shader.current = this;
        gl.useProgram(this.id);
    };

    Shader.getShader = function (name, type) {
        var deferred = Q.defer(),
            gl = Game.gl;

        Game.Util.readFile("./shaders/" + name)
            .then(function (source) {
                var shaderId;

                if (type == "fs" || type == "frag" || type == "fragment") {
                    shaderId = gl.createShader(gl.FRAGMENT_SHADER);
                } else if (type == "vs" || type == "vert" || type == "vertex") {
                    shaderId = gl.createShader(gl.VERTEX_SHADER);
                } else {
                    deferred.reject();
                }

                gl.shaderSource(shaderId, source);

                gl.compileShader(shaderId);
                if (!gl.getShaderParameter(shaderId, gl.COMPILE_STATUS)) {
                    console.log("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shaderId));
                    deferred.reject();
                }

                deferred.resolve({
                    "id" : shaderId,
                    "uniforms" : Shader.getUniforms(source),
                    "attributes" : Shader.getAttributes(source)
                });
            }, function (err) {
                console.log(err);
                throw err;
            });

        return deferred.promise;
    };

    Shader.getUniforms = function (source) {
        var regex = /(uniform)\s(\w*)\s(\w*)/g;
        var uniforms = [];

        var match = regex.exec(source);
        while (match) {
            uniforms.push(match[3]);
            match = regex.exec(source);
        }

        return uniforms;
    };

    Shader.getAttributes = function (source) {
        var regex = /(attribute)\s(\w*)\s(\w*)/g;
        var attributes = [];

        var match = regex.exec(source);
        while(match) {
            attributes.push(match[3]);
            match = regex.exec(source);
        }

        return attributes;
    };

    Game.Shader = Shader;
})();
