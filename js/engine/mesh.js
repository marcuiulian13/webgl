(function () {
    "use strict";

    var Mesh = function (vbo, cbo, nbo, ibo, size) {
        this.vbo = vbo;
        this.cbo = cbo;
        this.nbo = nbo;
        this.ibo = ibo;
        this.size = size;
    };

    Mesh.generateFromArray = function (vertices, colors, normals, indices) {
        return Mesh.generateFromArrayObject(
            vertices ? new Float32Array(vertices) : undefined,
            colors ? new Float32Array(colors) : undefined,
            normals ? new Float32Array(normals) : undefined,
            indices ? new Uint16Array(indices) : undefined
        )
    };

    Mesh.generateFromArrayObject = function (vertices, colors, normals, indices) {
        var gl = Game.gl;

        var size = indices.length;

        if (vertices) {
            var vbo = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
            gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
        }

        if (colors) {
            var cbo = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, cbo);
            gl.bufferData(gl.ARRAY_BUFFER, colors, gl.STATIC_DRAW);
        }

        if (normals) {
            var nbo = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, nbo);
            gl.bufferData(gl.ARRAY_BUFFER, normals, gl.STATIC_DRAW);
        }

        if (indices) {
            var ibo = gl.createBuffer();
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo);
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);
        }

        return new Mesh(vbo, cbo, nbo, ibo, size);
    };

    Game.Mesh = Mesh;
})();
