(function () {
    "use strict";

    var Model = function (mesh) {
        this.mesh = mesh;
        this.transform = Matrix.I(4);
    };

    Model.prototype.draw = function () {
        var gl = Game.gl,
            mesh = this.mesh;

        var shader = Game.Shader.current;

        gl.enableVertexAttribArray(shader.getAttribute("aPosition"));
        gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vbo);
        gl.vertexAttribPointer(shader.getAttribute("aPosition"), 3, gl.FLOAT, false, 0, 0);

        gl.enableVertexAttribArray(shader.getAttribute("aColor"));
        gl.bindBuffer(gl.ARRAY_BUFFER, mesh.cbo);
        gl.vertexAttribPointer(shader.getAttribute("aColor"), 3, gl.FLOAT, false, 0, 0);

        gl.enableVertexAttribArray(shader.getAttribute("aNormal"));
        gl.bindBuffer(gl.ARRAY_BUFFER, mesh.nbo);
        gl.vertexAttribPointer(shader.getAttribute("aNormal"), 3, gl.FLOAT, false, 0, 0);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, mesh.ibo);
        gl.drawElements(Game.drawMode, mesh.size, gl.UNSIGNED_SHORT, 0);
    };

    Model.prototype.setUniforms = function () {
        var gl = Game.gl,
            pMatrix = Game.camera.getProjMatrix(),
            vMatrix = Game.camera.getViewMatrix(),
            mMatrix = this.transform;

        var shader = Game.Shader.current;

        gl.uniformMatrix4fv(shader.getUniform("uPMatrix"), false, new Float32Array(pMatrix.flatten()));
        gl.uniformMatrix4fv(shader.getUniform("uVMatrix"), false, new Float32Array(vMatrix.flatten()));
        gl.uniformMatrix4fv(shader.getUniform("uMMatrix"), false, new Float32Array(mMatrix.flatten()));
        gl.uniformMatrix4fv(shader.getUniform("uIVMatrix"), false, new Float32Array(vMatrix.inverse().flatten()));

        gl.uniform1f(shader.getUniform("uTime"), Game.time);

        var ambient = Game.Lights.ambient;
        gl.uniform1f(shader.getUniform("uAmbientWeight"), ambient.weight);
        gl.uniform3fv(shader.getUniform("uAmbientColor"), new Float32Array(ambient.color));

        var directional = Game.Lights.directional;
        gl.uniform3fv(shader.getUniform("uDLightDir"), new Float32Array(directional.dir));
        gl.uniform3fv(shader.getUniform("uDLightColor"), new Float32Array(directional.color));
    };

    Game.Model = Model;
})();