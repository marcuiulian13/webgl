(function () {
    "use strict";

    var Camera = function () {
        this.fov = 80;
        this.near = 0.01;
        this.far = 500;

        this._perspective = makePerspective(this.fov, Game.WIDTH / Game.HEIGHT, this.near, this.far);

        this._rotation = { x: 0, y: 0, z: 0 };
        this._position = { x: 0, y: -10, z: 0 };

        this.frustum = new Game.Frustum(this.getProjMatrix(), this.getViewMatrix());
    };

    Camera.prototype.update = function () {
        var input = Game.Input;

        if(input.isKeyDown(Game.Input.KEYS.LEFT)) {
            this.rotate([0, -0.05, 0]);
        }
        if(input.isKeyDown(Game.Input.KEYS.RIGHT)) {
            this.rotate([0, 0.05, 0]);
        }
        if(input.isKeyDown(Game.Input.KEYS.UP)) {
            this.rotate([-0.05, 0, 0]);
        }
        if(input.isKeyDown(Game.Input.KEYS.DOWN)) {
            this.rotate([0.05, 0, 0]);
        }

        if(input.isKeyDown(Game.Input.KEYS.A)) {
            var dx = Math.cos(this._rotation.y);
            var dz = Math.sin(this._rotation.y);

            this.translate([dx * 0.5, 0, dz * 0.5]);
        }
        if(input.isKeyDown(Game.Input.KEYS.D)) {
            var dx = -Math.cos(this._rotation.y);
            var dz = -Math.sin(this._rotation.y);

            this.translate([dx * 0.5, 0, dz * 0.5]);
        }
        if(input.isKeyDown(Game.Input.KEYS.W)) {
            var dx = -Math.sin(this._rotation.y);
            var dz = Math.cos(this._rotation.y);

            this.translate([dx * 0.5, 0, dz * 0.5]);
        }
        if(input.isKeyDown(Game.Input.KEYS.S)) {
            var dx = Math.sin(this._rotation.y);
            var dz = -Math.cos(this._rotation.y);

            this.translate([dx * 0.5, 0, dz * 0.5]);
        }
        if(input.isKeyDown(Game.Input.KEYS.Q)) {
            this.translate([0, 0.5, 0]);
        }
        if(input.isKeyDown(Game.Input.KEYS.E)) {
            this.translate([0, -0.5, 0]);
        }

        this.frustum.update(this.getViewMatrix(), this.getProjMatrix());
    };

    Camera.prototype.getViewMatrix = function () {
        var rot = this._rotation,
            trans = this._position;

        if(this._update || !this._view) {
            this._update = false;

            this._view = Matrix.I(4)
                .x(Matrix.RotationX(rot.x).ensure4x4())
                .x(Matrix.RotationY(rot.y).ensure4x4())
                .x(Matrix.RotationZ(rot.z).ensure4x4())
                .x(Matrix.Translation($V([trans.x, trans.y, trans.z])).ensure4x4());
        }

        return this._view;
    };

    Camera.prototype.getProjMatrix = function () {
        return this._perspective;
    };

    Camera.prototype.rotate = function (v) {
        this._update = true;

        this._rotation.x += v[0];
        this._rotation.x = Math.min(Math.PI / 2, Math.max(this._rotation.x, -Math.PI / 2));

        this._rotation.y += v[1];
        this._rotation.y %= 360;

        this._rotation.z += v[2];
        this._rotation.z %= 360;
    };

    Camera.prototype.translate = function (v) {
        this._update = true;

        this._position.x += v[0];
        this._position.y += v[1];
        this._position.z += v[2];
    };

    Game.Camera = Camera;
})();