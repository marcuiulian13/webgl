(function () {
    "use strict";

    var Frustum = function (proj, view) {
        this._planes = [];
        this.update(proj, view);
    };

    Frustum.LEFT = 0;
    Frustum.RIGHT = 1;
    Frustum.BOTTOM = 2;
    Frustum.TOP = 3;
    Frustum.NEAR = 4;
    Frustum.FAR = 5;

    Frustum.prototype._normalizePlane = function (side) {
        var plane = this._planes[side];

        var magnitude = Math.sqrt(
            plane.e(1) * plane.e(1) +
            plane.e(2) * plane.e(2) +
            plane.e(3) * plane.e(3)
        );

        this._planes[side] = plane.x(1 / magnitude);
    };

    Frustum.prototype.update = function (proj, view) {
        var clipMatrix = proj.x(view);

        // This will extract the LEFT side of the frustum
        this._planes[Frustum.LEFT] = $V([
            clipMatrix.e(1, 4) + clipMatrix.e(1, 1),
            clipMatrix.e(2, 4) + clipMatrix.e(2, 1),
            clipMatrix.e(3, 4) + clipMatrix.e(3, 1),
            clipMatrix.e(4, 4) + clipMatrix.e(4, 1)
        ]);
        this._normalizePlane(Frustum.LEFT);

        // This will extract the RIGHT side of the frustum
        this._planes[Frustum.RIGHT] = $V([
            clipMatrix.e(1, 4) - clipMatrix.e(1, 1),
            clipMatrix.e(2, 4) - clipMatrix.e(2, 1),
            clipMatrix.e(3, 4) - clipMatrix.e(3, 1),
            clipMatrix.e(4, 4) - clipMatrix.e(4, 1)
        ]);
        this._normalizePlane(Frustum.RIGHT);

        // This will extract the BOTTOM side of the frustum
        this._planes[Frustum.BOTTOM] = $V([
            clipMatrix.e(1, 4) + clipMatrix.e(1, 2),
            clipMatrix.e(2, 4) + clipMatrix.e(2, 2),
            clipMatrix.e(3, 4) + clipMatrix.e(3, 2),
            clipMatrix.e(4, 4) + clipMatrix.e(4, 2)
        ]);
        this._normalizePlane(Frustum.BOTTOM);

        // This will extract the TOP side of the frustum
        this._planes[Frustum.TOP] = $V([
            clipMatrix.e(1, 4) - clipMatrix.e(1, 2),
            clipMatrix.e(2, 4) - clipMatrix.e(2, 2),
            clipMatrix.e(3, 4) - clipMatrix.e(3, 2),
            clipMatrix.e(4, 4) - clipMatrix.e(4, 2)
        ]);
        this._normalizePlane(Frustum.TOP);

        // This will extract the NEAR side of the frustum
        this._planes[Frustum.NEAR] = $V([
            clipMatrix.e(1, 4) + clipMatrix.e(1, 3),
            clipMatrix.e(2, 4) + clipMatrix.e(2, 3),
            clipMatrix.e(3, 4) + clipMatrix.e(3, 3),
            clipMatrix.e(4, 4) + clipMatrix.e(4, 3)
        ]);
        this._normalizePlane(Frustum.NEAR);

        // This will extract the FAR side of the frustum
        this._planes[Frustum.FAR] = $V([
            clipMatrix.e(1, 4) - clipMatrix.e(1, 3),
            clipMatrix.e(2, 4) - clipMatrix.e(2, 3),
            clipMatrix.e(3, 4) - clipMatrix.e(3, 3),
            clipMatrix.e(4, 4) - clipMatrix.e(4, 3)
        ]);
        this._normalizePlane(Frustum.FAR);
    };

    Frustum.prototype._distance = function (side, point) {
        var plane = this._planes[side];

        if (plane) {
            return plane.e(1) * point.e(1) + plane.e(2) * point.e(2) + plane.e(3) * point.e(3) + plane.e(4);
        }

        return 0;
    };

    Frustum.prototype.inside = function (point) {
        for(var i = 0; i < 6; i++) {
            if(this._distance(i, point) < 0)
                return false;
        }

        return true;
    };

    Game.Frustum = Frustum;
})();