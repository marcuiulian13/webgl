attribute vec3 aPosition;

uniform mat4 uIPMatrix;
uniform mat4 uTVMatrix;

varying vec3 vEyeDir;

void main(void) {
    vec3 unprojected = (uIPMatrix * vec4(aPosition, 1.0)).xyz;
    vEyeDir = mat3(uTVMatrix) * unprojected;

    gl_Position = vec4(aPosition, 1.0);
}