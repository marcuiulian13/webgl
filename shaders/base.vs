attribute vec3 aPosition;
attribute vec4 aColor;

uniform mat4 uPMatrix;
uniform mat4 uVMatrix;
uniform mat4 uMMatrix;
uniform float uTime;

varying vec4 vColor;
varying float vTime;

void main(void) {
    vTime = uTime;
    vColor = aColor;

    gl_Position = uPMatrix * uVMatrix * uMMatrix * vec4(aPosition, 1.0);
}