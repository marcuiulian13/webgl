// Atrributes
attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec3 aColor;

// Uniforms
uniform mat4 uPMatrix;
uniform mat4 uVMatrix;
uniform mat4 uMMatrix;
uniform float uTime;

uniform float uAmbientWeight;
uniform vec3 uAmbientColor;

uniform vec3 uDLightDir;
uniform vec3 uDLightColor;

uniform mat4 uIVMatrix;

// Varyings
varying vec3 vPosition;
varying vec4 vColor;
varying float vTime;
varying vec3 vEyePos;

void main(void) {
    vec3 ambientColor = uAmbientColor * aColor;
    vec3 diffuseColor = uDLightColor * max(dot(aNormal, uDLightDir), 0.0) * aColor;

    vec3 finalColor = ambientColor * uAmbientWeight + (diffuseColor) * (1.0 - uAmbientWeight);

    vColor = vec4(finalColor, 1.0);
    vTime = uTime;

    vEyePos = (uIVMatrix * vec4(0.0, 0.0, 0.0, 1.0)).xyz;

    vec4 position = uMMatrix * vec4(aPosition, 1.0);
    vPosition = position.xyz;

    gl_Position = uPMatrix * uVMatrix * position;
}