// Atrributes
attribute vec3 aPosition;
attribute vec3 aNormal;
attribute vec3 aColor;

// Uniforms
uniform mat4 uPMatrix;
uniform mat4 uVMatrix;
uniform mat4 uMMatrix;
uniform float uTime;

uniform mat4 uIPMatrix;
uniform mat4 uIVMatrix;
uniform mat4 uTVMatrix;

// Varyings
varying vec3 vPosition;
varying vec3 vNormal;
varying vec3 vColor;

varying vec4 vViewPos;
varying vec3 vEyePos;

varying float vTime;

void main(void) {
    vNormal = aNormal;
    vColor = aColor;
    vTime = uTime;

    vEyePos = (uIVMatrix * vec4(0.0, 0.0, 0.0, 1.0)).xyz;

    vPosition = (uMMatrix * vec4(aPosition, 1.0)).xyz;
    vViewPos = uVMatrix * vec4(vPosition, 1.0);

    gl_Position = uPMatrix * vViewPos;
}