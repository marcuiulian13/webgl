precision highp float;

uniform float uAmbientWeight;
uniform vec3 uAmbientColor;

uniform vec3 uDLightDir;
uniform vec3 uDLightColor;

varying vec3 vPosition;
varying vec3 vNormal;
varying vec3 vColor;

varying vec4 vViewPos;
varying vec3 vEyePos;

varying float vTime;

const vec3 fogColor = vec3(0.2, 0.2, 0.3);
const float fogDensity = 0.004;

vec3 calculateLight() {
    vec3 ambientColor = uAmbientColor * vColor;
    vec3 diffuseColor = uDLightColor * max(dot(vNormal, uDLightDir), 0.0) * vColor;
    return ambientColor * uAmbientWeight + (diffuseColor) * (1.0 - uAmbientWeight);
}

float calculateFog() {
    float dist = length(vViewPos);

    float fogFactor = 1.0 / exp((dist * fogDensity) * (dist * fogDensity));
    return clamp(fogFactor, 0.0, 1.0);
}

void main(void) {
    vec3 lightColor = calculateLight();
    float fogFactor = calculateFog();

    vec3 finalColor = mix(fogColor, lightColor, fogFactor);

    gl_FragColor = vec4(vNormal * finalColor, 1.0);
}