precision highp float;

varying vec3 vEyeDir;

void main(void) {
    float ang = dot(vEyeDir, vec3(0.0, 1.0, 0.0));
    gl_FragColor = vec4(0.2, 0.2, ang / 2.0 + 0.2, 1.0);
}